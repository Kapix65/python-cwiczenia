import math
from FileManager import FileManager


def get_list_even(a_list, b_list):
    result = []
    for x in range(1, len(a_list)):
        if (a_list[x] % 2) == 0:
            result.append(x)
    for x in range(1, len(b_list)):
        if not (b_list[x] % 2) == 0:
            result.append(x)
    return result


def get_info(text):
    return {
        'length': str(len(text)),
        'letters': set(list(text.replace(" ", ""))),
        'big_letters': text.upper(),
        'small_letters': text.lower(),
    }


print(get_info("dsadasdas"))


def delete_letters(text, letter):
    return text.replace(letter, "")


text = "fdsadasdaadasaaaaaaaaaaaaaaaaa"
text = delete_letters(text, "a")
print(text)


class Calculator:
    def add(self, a, b):
        return a + b;

    def diffrence(self, a, b):
        return a - b;

    def multiply(self, a, b):
        return a * b

    def divide(self, a, b):
        try:
            return a / b
        except ZeroDivisionError:
            print("Dont divide via 0")
            return a


class ScienceCalculator(Calculator):
    def power(self, a, b):
        return pow(a, b)

    def square(self, a):
        try:
            return math.sqrt(a)
        except:
            print("Square not possible")
            return a


MyCalculator = ScienceCalculator()

sum_dummy = MyCalculator.add(1, 2)
power_dummy = MyCalculator.power(2, 3)

print(sum_dummy)
print(power_dummy)


def reverse_string(text):
    return text[::-1]


reversed_string = reverse_string("abcdfghijklmnoprstwxyz")
print(reversed_string)

my_file = FileManager("Text")
my_file.add("XDXDXDXDXDXDX")
my_file.add("hahaha i wrote a emote")
print(my_file.read())
