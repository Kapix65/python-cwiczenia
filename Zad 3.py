text1 = '{} {}'.format('one', 'two')
print(text1)
text2 = '{:>10}'.format('wow format')
print(text2)
text3 = '{:.3}'.format('trzy')
print(text3)
text3 = {
    'Brand': 'Toyota',
    'Model': 'GT86'
}
formated_text3 = '{p[Brand]} {p[Model]}'.format(p=text3)
print(formated_text3)

text4 = '{:{prec}} = {:{prec}}'.format('Pi number', 3.14159265359, prec='.3')
print(text4)


class Text5(object):
    content = "Text 5's content"


print('{p.content}'.format(p=Text5()))
