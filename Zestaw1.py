import copy
import names
import random
from faker import Faker
from datetime import date

fake = Faker()
lorem_ipsum_var = 'Lorem Ipsum jest tekstem stosowanym jako przykładowy wypełniacz w przemyśle poligraficznym.\n' \
                  ' Został po raz pierwszy użyty w XV w. przez nieznanego drukarza do wypełnienia tekstem próbnej książki.\n ' \
                  'Pięć wieków później zaczął być używany przemyśle elektronicznym, pozostając praktycznie niezmienionym.\n' \
                  ' Spopularyzował się w latach 60. XX w. wraz z publikacją arkuszy Letrasetu,\n' \
                  ' zawierających fragmenty Lorem Ipsum, a ostatnio z zawierającym różne wersje\n' \
                  ' Lorem Ipsum oprogramowaniem przeznaczonym do realizacji druków na komputerach osobistych, jak Aldus PageMaker'

name = "Kacper"
last_name = "Zieliński"
tekst1 = f'W tekście lorem ipsum jest {lorem_ipsum_var.count(name[2])} liter "{name[2]}" oraz {lorem_ipsum_var.count(last_name[3])} liter "{last_name[3]}".'
print(tekst1)
text2 = 'War never changes'
print(dir(text2))
help(text2.replace('War', 'Life'))

name_reversed = name[::-1].lower().capitalize()
last_name_reversed = last_name[::-1].lower().capitalize()
print(name_reversed)
print(last_name_reversed)

list_original = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
print(list)
second_half = list_original[5:]
list_original = list_original[:5]
print(list_original)
print(second_half)
list_original = [0] + list_original + second_half
print("Połączona lista")
print(list_original)

list_copy = copy.deepcopy(list_original)
list_copy.sort(reverse=True)
print(list_copy)

list_of_students = []
for i in range(1, 21):
    list_of_students.append(
        (str(random.randint(140000, 160000)),
         names.get_full_name())
    )
print(list_of_students)
better_list_of_students = []
current_date = date.today()
current_year = current_date.year
for i in range(1, 21):
    name = names.get_full_name()
    age = random.randint(18, 23)
    better_list_of_students.append(
        {
            'name': name,
            'mail': f'{name.replace(" ", "_").lower()}{str(random.randint(1, 1000))}@gmail.com',
            'address': fake.address(),
            'Birth': str(current_year - age),
            'age': str(age),
        }
    )
print(better_list_of_students)

list_of_phone_numbers = []
for i in range(1, 30):
    fake_number = random.randint(100000000, 999999999)
    list_of_phone_numbers.append(fake_number)
    list_of_phone_numbers.append(fake_number)

set_of_phone_numbers = set(list_of_phone_numbers)
print(set_of_phone_numbers)

a = range(1, 10)
for i in a:
    print(i)
b = range(20, 101, 5)
b = b.__reversed__()
for i in b:
    print(i)

name = names.get_full_name()
age = 20
list_of_dictionary = [
    {
        'Brand': 'Toyota',
        'Model': 'Supra',
    },
    {
        'name': name,
        'mail': f'{name.replace(" ", "_").lower()}{str(random.randint(1, 1000))}@gmail.com',
        'address': fake.address(),
        'Birth': str(current_year - age),
        'age': str(age),
    },
    {
        'Language': 'Python',
        'Framework': 'Django',
    }
]
ultimate_string = ""
for dict in list_of_dictionary:
    for key, value in dict.items():
        string_to_add = f'{key}: {value}\n'
        ultimate_string += string_to_add
    ultimate_string += "\n"

print(ultimate_string)
